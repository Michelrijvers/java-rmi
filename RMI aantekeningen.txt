RMI aantekeningen:

1. Registry starten:
De registry moet de gecompileerde classbestanden van de applicatie kunnen vinden. Dat kan door een zgn. codebase op te geven. Dit kan een file zijn of een http- of ftp-server. Een file werkt alleen wanneer client en server bij hetzelfde filesysteem kunnen. Wanneer je een http- of ftp-server gebruikt is het handig om de code in een jar te zetten.
De codebase vanaf de registry kun je meegeven door: 

rmiregistry -J-Djava.rmi.server.codebase="file:/C:/dev/workspace/workspace/HelloServer/bin/"
rmiregistry 1099 -J-Djava.rmi.server.codebase="http://localhost/classes/"
rmiregistry 1099 -J-Djava.rmi.server.codebase="http://192.168.178.29/classes/bin"

Bij problemen met het bereiken van de registry: 
met 'nmap' (apart downloaden en installeren) kun je specifieke poorten op een IP-adres scannen
bv: nmap 145.48.6.147 -p1099



2. Server starten:
java -classpath .\bin example.hello.Server

Erg handig: binnen Eclipse of vanaf de command line extra debug informatie tonen.
Dit geeft vaak de bron van de fout aan.
-Djava.security.debug=access,failure

Wanneer je een RMI client en server op verschillende machines wilt runnen moet je applicatiee een SecurityManager installeren. Deze leest zijn security settings uit een tekstbestand. Hierin staan de security settings per regel uitgeschreven. Een handige manier om te testen of je applicatie daadwerkelijk het security policy bestand leest is om met opzet fouten in de tekst te zetten. De securitymanager zou dan moeten klagen.

Remote communicatie client - server
Om een remote server vanaf een client te kunnen bereiken moet je de setting java.rmi.server.hostname hebben ingesteld op het IP-addres of op de hostname van de server machine. Alleen 'localhost' is niet voldoende, omdat de registry deze setting gebruikt om tussen client en server te communiceren. De client kent namelijk de server-localhost niet, wel het address.

Let op dat je de registry ook het juiste server machine adres meegeeft in de code base. Zie de voorbeelden hierboven.

3. Client:
Direct runnen vanuit Eclipse, of vanaf de commandline met
java -classpath .\bin example.hello.Client
De client heeft geen codebase nodig.


Handige Java tool voor het instellen van security settings in een .policy file:
policytool
Runnen vanaf de commandline, start een GUI op.


Algemene opmerkingen

- de settings voor het runnen van de client en servers staan in de .properties bestanden in de resources map. Let op dat als je Gradle build gebruikt, de resources vanuit de 'werkmap' (bv C:\dev\workspace\) naar de map van de webserver (C:\xampp\htdocs) worden gekopieerd. Alle wijzigingen in de map van de webserver worden daarmee overschreven.


