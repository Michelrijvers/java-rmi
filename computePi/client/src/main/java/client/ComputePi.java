package client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.math.BigDecimal;
import compute.Compute;

/**
 * RMI client to compute Pi for a given precision.
 *
 */
public class ComputePi {
	
	/**
	 * Main method. 
	 * Initialize, 
	 * connect to RMI registry, 
	 * get the stub and 
	 * call the method.
	 * 
	 * @param args Command line arguments, [hostname] [precision]
	 */
	public static void main(String args[]) {

		String hostname = null;
		
		if(args.length != 2) {
			System.out.println("Usage: ComputePi [hostname] [precision]");
			System.exit(1);
		}
		
		if(args[0] != null) {
			hostname = args[0];
		}
		
		System.out.println("Starting ComputePi, host = " + hostname + ", precision = " + args[1]);
		
		// System.setProperty("java.rmi.server.hostname", hostname);
		// System.setProperty("java.rmi.server.codebase", "http://" + hostname + "/classes/ComputePi/");
		System.setProperty("java.security.policy", "http://" + hostname + "/classes/ComputePi/server.policy");

		System.out.println("Install the security manager");
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}

        try {
            String name = "Compute";
            System.out.println("Locate and initialize the registry");
            Registry registry = LocateRegistry.getRegistry();

            System.out.println("Lookup name " + name + " in the registry");
            Compute comp = (Compute) registry.lookup(name);

            System.out.println("Creating a new Pi task to be executed by the server");
            Pi task = new Pi(Integer.parseInt(args[1]));

            System.out.println("Sending task to server");
            BigDecimal pi = comp.executeTask(task);
            System.out.println("Result returned from the server: " + pi);
        } catch (Exception e) {
            System.err.println("ComputePi exception:");
            e.printStackTrace();
        }
    }    
}
